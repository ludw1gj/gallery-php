DROP DATABASE IF EXISTS gallery;
CREATE DATABASE gallery;
USE gallery;

CREATE TABLE artwork (
  artwork_id  SERIAL PRIMARY KEY,
  username    TEXT,
  title       TEXT,
  category    TEXT,
  description TEXT,
  tags        TEXT,
  filename    TEXT
);

CREATE TABLE member (
  member_id SERIAL PRIMARY KEY,
  username  VARCHAR(20) UNIQUE,
  password  CHAR(40),
  reg_date  DATETIME
);
