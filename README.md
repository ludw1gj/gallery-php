# Gallery PHP

An image gallery website named ImageHub created using PHP, Bootstrap, and MySQL in an MVC (Model, View, Controller) pattern. 

I decided to delve into PHP a bit more as I wanted to get a little more experience with it. In the final project of 
my university course Web Programming, I had to create a website (using PHP) that allowed registered users to upload 
images for display, while users who have not registered/logged in can view the website but can't upload images. In this 
repo I modified the project's code into a more MVC style of structuring a websites codebase.

## Requirements
* `php72`
* `mysql`

## Database Configuration
If needed, change the `mysqli_connect` parameter values in the Database class constructor to match your setup. 
```
CREATE DATABASE gallery;
USE gallery;

CREATE TABLE artwork (
  artwork_id  SERIAL PRIMARY KEY,
  username    TEXT,
  title       TEXT,
  category    TEXT,
  description TEXT,
  tags        TEXT,
  filename    TEXT
);

CREATE TABLE member (
  member_id SERIAL PRIMARY KEY,
  username  VARCHAR(20) UNIQUE,
  password  CHAR(40),
  reg_date  DATETIME
);
```

## Preview
![Image of ImageHub Preview](https://github.com/robertjeffs/gallery-php/blob/master/preview.png?raw=true)