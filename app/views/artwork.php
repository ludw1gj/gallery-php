<h2>Artwork</h2>

<?php
$artwork = $viewData["artwork"];
echo "<h3><b>{$artwork->title}</b></h3>
    <h4>By <a href='/member?member={$artwork->username}'>{$artwork->username}</a></h4>
    <p><b>Category:</b> {$artwork->category}</p>
    <img id='artwork-full' src='resource/artworks/{$artwork->filename}' alt='{$artwork->filename}'>
    <p><b>Description:</b><br>{$artwork->description}</p>
    <p><b>Tags:</b> {$artwork->tags}</p>";
?>
