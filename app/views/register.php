<h3>Register</h3>

<form method=post action="/process-registration">
    <div class="form-group">
        <label for="registrationUsername">Username:</label>
        <input class="form-control" id="registrationUsername" type="text" name="username" placeholder="Username">
    </div>
    <div class="form-group">
        <label for="registrationPassword">Password:</label>
        <input class="form-control" id="registrationPassword" type="password" name="password"
               placeholder="Password"><br>
    </div>
    <button class="btn btn-default" type="submit">Register</button>
</form>
