<h3>Member Page</h3>

<?php
echo "<h4>{$_GET["member"]}'s Artwork</h4>";

if (gettype($viewData["artworks"]) === "array") {
    foreach ($viewData["artworks"] as $artwork) {
        echo "<a href='artwork?id={$artwork->id}'>
            <img class='artwork-preview' src='resource/artworks/{$artwork->filename}' alt='{$artwork->filename}' height='200' width='200'>
          </a>";
    }
} else {
    $artwork = $viewData["artworks"];
    echo "<a href='artwork?id={$artwork->id}'>
            <img class='artwork-preview' src='resource/artworks/{$artwork->filename}' alt='{$artwork->filename}' height='200' width='200'>
          </a>";
}

?>
