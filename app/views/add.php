<h3>Add New Image</h3>

<form method="post" action="/process-new-artwork" enctype="multipart/form-data">
    <div class="form-group">
        <label for="imageTitle">Title:</label>
        <input class="form-control" id="imageTitle" type="text" name="title" placeholder="Title">
    </div>
    <div class="form-group">
        <label for="imageCategory">Category:</label>
        <input class="form-control" id="imageCategory" type="text" name="category" placeholder="Category"><br>
    </div>
    <div class="form-group">
        <label for="imageDescription">Description:</label>
        <textarea class="form-control" id="imageDescription" name="description" cols="50" rows="5"
                  placeholder="Description"></textarea>
    </div>
    <div class="form-group">
        <label for="imageTags">Tags:</label>
        <input class="form-control" id="imageTags" type="text" name="tags" placeholder="Tags"><br>
    </div>
    <div class="form-group">
        <label id="imageFile">Image File</label>
        <input type="file" name="file_upload" id="imageFile"><br>
    </div>
    <button class="btn btn-default" type="submit">Submit New Image</button>
</form>
