<h3>Login</h3>

<form method=post action="/process-registration">
    <div class="form-group">
        <label for="loginUsername">Username:</label>
        <input class="form-control" id="loginUsername" type="text" name="username" placeholder="Username">
    </div>
    <div class="form-group">
        <label for="loginPassword">Password:</label>
        <input class="form-control" id="loginPassword" type="password" name="password" placeholder="Password"><br>
    </div>
    <button class="btn btn-default" type="submit">Login</button>
</form>
