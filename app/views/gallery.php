<h4>Gallery</h4>

<select id="category-menu" onchange="categoryMenu();">
    <option value>Categories...</option>
    <option value="gallery?category=">Any</option>
    <?php
    if ($viewData["categories"] !== null) {
        foreach ($viewData["categories"] as $cateogry) {
            echo "<option value='/gallery?category={$cateogry}'>{$cateogry}</option>";
        }
    }
    ?>
</select><br>

<?php
if ($viewData["artworks"] !== null) {
    foreach ($viewData["artworks"] as $artwork) {
        echo "<a href='/artwork?id={$artwork->id}'>
            <img class='artwork-preview' src='resource/artworks/{$artwork->filename}' alt='{$artwork->filename}' height='200' width='200'>
          </a>";
    }
} else {
    $artwork = $viewData["artworks"];
    echo "<a href='/artwork?id={$artwork->id}'>
            <img class='artwork-preview' src='resource/artworks/{$artwork->filename}' alt='{$artwork->filename}' height='200' width='200'>
          </a>";
}
?>
