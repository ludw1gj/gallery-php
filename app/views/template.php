<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?> | Gallery</title>

    <link type="text/css" rel="stylesheet" href="/assets/styles/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="/assets/scripts/script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#gallery-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">ImageHub</a>
        </div>

        <div class="collapse navbar-collapse" id="gallery-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>
                <li><a href="/gallery?category=">Gallery</a></li>
                <?php
                if (isset($_SESSION["username"])) {
                    echo "<li><a href='/new-artwork'>Add New Artwork</a></li>";
                }
                ?>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php
                if (isset($_SESSION["username"])) {
                    echo "<li><a href='#'>{$_SESSION['username']} <i class='fa fa-user-plus'></i></a></li>
                          <li><a href='/process-logout'>Logout <i class='fa fa-user-plus'></i></a></li>";
                } else {
                    echo "<li><a href='/register'>Sign Up <i class='fa fa-user-plus'></i></a></li>
                          <li><a href='/login'>Login <i class='fa fa-user'></i></a></li>";
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

<main>
    <div class="container">
        <?php include_once($viewPath); ?>
    </div>
</main>

<footer>
    <div class="container">
        <hr>
        <p>©2018 Robert Ludwig Jeffs</p>
    </div>
</footer>
</body>
</html>
