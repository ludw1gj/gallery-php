<div class="container">
    <h1>Welcome to ImageHub</h1>
    <p class='lead'>The place for sharing images or memes.</p>
    <hr>
</div>

<!-- Carousel -->
<div id="carousel-home-page" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php
        if ($viewData["filenames"] !== null) {
            for ($i = 0; $i < count($viewData["filenames"]); $i++) {
                if ($i === 0) {
                    echo "<li data-target='#carousel-home-page' data-slide-to='0' class='active'></li>";
                    $first = false;
                } else {
                    echo "<li data-target='#carousel-home-page' data-slide-to='{$i}' class=''></li>";
                }
            }
        }
        ?>
    </ol>
    <div class="carousel-inner">
        <?php
        if ($viewData["filenames"] !== null) {
            $first = true;
            foreach ($viewData["filenames"] as $filename) {
                if ($first) {
                    echo "<div class='item active'>
                        <img id='carousel-img' alt='{$filename}' src='resource/artworks/{$filename}'>
                       </div>";
                    $first = false;
                } else {
                    echo "<div class='item'>
                    <img id='carousel-img' alt='{$filename}' src='resource/artworks/{$filename}'>
                  </div>";
                }
            }
        }
        ?>
    </div>

    <a class="left carousel-control" href="#carousel-home-page" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-home-page" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<!-- Members -->
<h3>Members who have uploaded artwork:</h3>

<ul class='list-inline'">
<?php
if ($viewData["usernames"] !== null) {
    foreach ($viewData["usernames"] as $username) {
        echo "<li><a href='member?member={$username}'>{$username}</a></li>";
    }
} else {
    echo "<p>No Members have uploaded artwork.</p>";
}
?>
</ul>
