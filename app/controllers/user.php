<?php

function processUserRegistration()
{
    $username = $_POST["username"];
    $password = $_POST["password"];
    createNewMember($username, $password);

    $_SESSION["username"] = $username;
    header("location:/");
}

function processUserLogin()
{
    $username = $_POST["username"];
    $password = $_POST["password"];

    if (validateMember($username, $password)) {
        $_SESSION["username"] = $username;

        header("location:/");
        exit(0);
    } else {
        header("location:/login");
        exit(0);
    }
}

function processUserLogout()
{
    unset($_SESSION["username"]);
    header("Location:/");
}


function processNewUserArtwork()
{
    redirectUserToLoginPageIfNotLoggedIn();

    $artworkFileName = $_FILES["file_upload"]["name"];
    if (!move_uploaded_file($_FILES["file_upload"]["tmp_name"], "resource/artworks/{$artworkFileName}")) {
        renderError("error uploading file");
        return;
    }

    createArtwork($_SESSION["username"], $_POST["title"], $_POST["category"], $_POST["description"], $_POST["tags"], $artworkFileName);
    header("location:/");
}
