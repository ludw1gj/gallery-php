<?php

function render($title, $viewPath, $viewData)
{
    include_once("app/views/template.php");
}

function renderError($errorMessage)
{
    http_response_code(500);
    render("Error", "app/views/error.php", array(
        "error" => $errorMessage
    ));
}

function render404Page()
{
    http_response_code(404);
    render("404", "app/views/404.php", null);
}


function renderHomePage()
{
    render("Home", "app/views/home.php", array(
        "filenames" => getFourMostRecentArtworkFilenames(),
        "usernames" => getMembersUsernamesWhoUploadedArtwork()
    ));
}

function renderLoginPage()
{
    redirectUserToHomePageIfLoggedIn();
    render("Login", "app/views/login.php", null);
}

function renderRegisterPage()
{
    redirectUserToHomePageIfLoggedIn();
    render("Register", "app/views/register.php", null);
}

function renderMemberPage()
{
    $username = $_GET["member"];
    render($username, "app/views/member.php", array(
        "artworks" => getArtworksByUsername($username)
    ));
}

function renderGalleryPage()
{
    $category = $_GET["category"];
    if ($category) {
        $title = ucwords($category);
        $artworks = getArtworksByCategory($category);
    } else {
        $title = "Any";
        $artworks = getAllArtworks();
    }

    render($title, "app/views/gallery.php",
        array(
            "categories" => getArtworkCategories(),
            "artworks" => $artworks
        ));
}

function renderArtworkPage()
{
    $artwork = getArtworkById($_GET["id"]);
    if ($artwork === null) {
        renderError("Artwork not found.");
        return;
    }

    render($artwork->title, "app/views/artwork.php", array(
        "artwork" => $artwork
    ));
}

function renderAddNewArtworkPage()
{
    redirectUserToLoginPageIfNotLoggedIn();
    render("Add New Artwork", "app/views/add.php", null);
}
