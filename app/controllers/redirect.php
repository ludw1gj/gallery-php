<?php

function redirectUserToLoginPageIfNotLoggedIn()
{
    if (!isset($_SESSION["username"])) {
        header("Location:/login");
        exit(0);
    }
}

function redirectUserToHomePageIfLoggedIn()
{
    if (isset($_SESSION["username"])) {
        header("Location:/");
        exit(0);
    }
}
