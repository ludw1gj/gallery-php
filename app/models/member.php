<?php

class Member
{
    public $id;
    public $username;
    public $password;

    public function __construct($id, $username, $password)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }
}

/**
 * Create a member and adds it to the database
 * @param string $username The member's username
 * @param string $password The member's password
 * @return void
 */
function createNewMember($username, $password)
{
    $db = Database::getInstance();
    $db->query("INSERT INTO member VALUES(null, '$username', SHA('$password'), now())");
}

/**
 * Validates a member
 * @param string $username The member's username
 * @param string $password The member's password
 * @return bool Returns true if member is valid
 */
function validateMember($username, $password)
{
    $db = Database::getInstance();
    $result = $db->query("SELECT * FROM member WHERE username = '$username' AND password = SHA('$password')");
    return mysqli_num_rows($result) > 0;
}

/**
 * Get member's usernames who have uploaded artwork
 * @return array|null Returns array string of usernames or null
 */
function getMembersUsernamesWhoUploadedArtwork()
{
    $result = Database::getInstance()->query("SELECT DISTINCT username FROM artwork");

    if (mysqli_num_rows($result) === 0) {
        return null;
    }

    $usernames = [];
    while ($username = mysqli_fetch_array($result)) {
        $usernames[] = $username["username"];
    }
    return $usernames;
}
