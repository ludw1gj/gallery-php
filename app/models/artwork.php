<?php

class Artwork
{
    public $id;
    public $username;
    public $title;
    public $category;
    public $description;
    public $tags;
    public $filename;

    public function __construct($id, $username, $title, $category, $description, $tags, $filename)
    {
        $this->id = $id;
        $this->username = $username;
        $this->title = $title;
        $this->category = $category;
        $this->description = $description;
        $this->tags = $tags;
        $this->filename = $filename;
    }
}

/**
 * Get artworks from the database by SQL query
 * @param string $query An SQL query
 * @return array|null Returns array of Artwork or null
 */
function getArtworks($query = "")
{
    $result = Database::getInstance()->query($query);
    if (mysqli_num_rows($result) === 0) {
        return null;
    }

    if (mysqli_num_rows($result) === 1) {
        $artwork = mysqli_fetch_array($result);
        return array(new Artwork($artwork["artwork_id"], $artwork["username"], $artwork["title"], $artwork["category"],
            $artwork["description"], $artwork["tags"], $artwork["filename"]));
    }

    $artworks = [];
    while ($row = mysqli_fetch_array($result)) {
        $artworks[] = new Artwork($row["artwork_id"], $row["username"], $row["title"], $row["category"],
            $row["description"], $row["tags"], $row["filename"]);
    }
    return $artworks;
}

/**
 * Create an artwork and adds it to the database
 * @param $username string
 * @param $imageTitle string
 * @param $imageCategory string
 * @param $imageDescription string
 * @param $imageTags string
 * @param $imageFileName string
 * @return void
 */
function createArtwork($username, $imageTitle, $imageCategory, $imageDescription, $imageTags, $imageFileName)
{
    $db = Database::getInstance();
    $db->query("INSERT INTO artwork VALUES(NULL, '$username', '$imageTitle', '$imageCategory', '$imageDescription', '$imageTags', '$imageFileName')");
}

/**
 * Get all artworks from the database
 * @return array|null Returns array of Artwork or null
 */
function getAllArtworks()
{
    return getArtworks("SELECT * FROM artwork ORDER BY artwork_id DESC");
}

/**
 * Get an artwork by ID from the database
 * @param int $id Artwork's ID
 * @return Artwork|null Returns Artwork or null
 */
function getArtworkById($id)
{
    $artwork = getArtworks("SELECT * FROM artwork WHERE artwork_id = {$id}");
    if ($artwork === null) {
        return null;
    }
    return $artwork[0];
}

/**
 * Get artworks by username from the database
 * @param string $username Username of the uploader of the artwork
 * @return array|null Returns array of Artwork or null
 */
function getArtworksByUsername($username)
{
    return getArtworks("SELECT * FROM artwork WHERE username = '$username' ORDER BY artwork_id DESC");
}

/**
 * Get artworks by category from the database
 * @param string $category Category of artwork
 * @return array|null Returns array of Artwork or null
 */
function getArtworksByCategory($category)
{
    return getArtworks("SELECT * FROM artwork WHERE category = '{$category}' ORDER BY artwork_id DESC");
}

/**
 * Get four most recent artwork file names from the database
 * @return array|null Returns array string of filenames or null
 */
function getFourMostRecentArtworkFilenames()
{
    $result = Database::getInstance()->query("SELECT filename FROM artwork ORDER BY artwork_id DESC LIMIT 4");

    if (mysqli_num_rows($result) === 0) {
        return null;
    }

    $filenames = [];
    while ($row = mysqli_fetch_array($result)) {
        $filenames[] = $row["filename"];
    }
    return $filenames;
}

/**
 * Get all distinct artwork categories from the database
 * @return array|null Returns array string of categories or null
 */
function getArtworkCategories()
{
    $result = Database::getInstance()->query("SELECT DISTINCT category FROM artwork");

    if (mysqli_num_rows($result) === 0) {
        return null;
    }

    $categories = [];
    while ($row = mysqli_fetch_array($result)) {
        $categories[] = $row["category"];
    }
    return $categories;

}
