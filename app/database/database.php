<?php

class Database
{
    private static $instance = null;
    private $db;

    private function __construct()
    {
        $this->db = mysqli_connect("127.0.0.1", "root", "", "gallery") or die("Could not connect to database");
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    /**
     * Query the database
     * @param string $query An SQL query
     * @return bool|mysqli_result
     */
    public function query($query = "")
    {
        return mysqli_query($this->db, $query);
    }
}
