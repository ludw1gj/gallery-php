<?php

require_once("app/database/database.php");

require_once("app/models/artwork.php");
require_once("app/models/member.php");

require_once("app/controllers/site.php");
require_once("app/controllers/user.php");
require_once("app/controllers/redirect.php");

function router()
{
    session_start();
    $url = isset($_SERVER["REQUEST_URI"]) ? $_SERVER["REQUEST_URI"] : "/";
    $page = strtok($url, "?");

    switch ($page) {
        case "/":
            renderHomePage();
            break;
        case "/register":
            renderRegisterPage();
            break;
        case "/process-registration":
            processUserRegistration();
            break;
        case "/login":
            renderLoginPage();
            break;
        case "/process-login":
            processUserLogin();
            break;
        case "/process-logout":
            processUserLogout();
            break;
        case "/member":
            renderMemberPage();
            break;
        case "/gallery":
            renderGalleryPage();
            break;
        case "/artwork":
            renderArtworkPage();
            break;
        case "/new-artwork":
            renderAddNewArtworkPage();
            break;
        case "/process-new-artwork":
            processNewUserArtwork();
            break;
        default:
            render404Page();
            break;
    }
}

router();
