function categoryMenu() {
    const menu = document.getElementById("category-menu");
    const selected = menu.options.selectedIndex;
    location.href = menu.options[selected].value;
}